@echo off
echo ANY DAMAGE DONE TO YOUR PC IS NOT MY RESPONSIBILITY
pause
cls
:start
color 0b

echo 						Welcome to Bug-Blitz
echo.
echo 						1 [ Delete Temporary Files ]
echo.
echo 						2 [ Run Disk-Cleanup ]
echo.
echo 						3 [ Fix System Errors ]
echo.
echo 						4 [ Disable Background Apps ]
echo.
echo 						5 [ Install Apps {chocolatey} ]
echo.
echo 						6 [Run WinUtil {Advanced users only}]
set /p tweaks=Input: 
if %tweaks% == 1 goto tweak1
if %tweaks% == 2 goto tweak2
if %tweaks% == 3 goto tweak3
if %tweaks% == 4 goto tweak4
if %tweaks% == 5 goto tweak5
if %tweaks% == 6 goto tweak6
:tweak1 
del C:\Windows\Prefetch\*.* /Q
del /q/f/s %TEMP%\*
echo Tweak completed successfully
pause
cls
goto start
:tweak2 
cleanmgr
chkdsk
echo Tweak completed successfully
pause
cls
goto start
:tweak3
sfc /scannow
DISM /Online /Cleanup-Image /CheckHealth
echo Tweak completed successfully
pause
cls
goto start
:tweak4
Reg Add HKCU\Software\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications /v GlobalUserDisabled /t REG_DWORD /d 1 /f
echo Tweak completed successfully
pause
cls
goto start
:tweak5
set /p app=Enter app name: 
powershell Add-AppxPackage -RegisterByFamilyName -MainPackage Microsoft.DesktopAppInstaller_8wekyb3d8bbwe
powershell winget install %app%
pause
cls
goto start
:tweak6
powershell iwr -useb https://christitus.com/win | iex
pause
cls
goto start
