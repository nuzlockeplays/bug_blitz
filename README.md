# Welcome to Bug-Blitz

## What is Bug-Blitz?

Bug-Blitz is a BAT ( batch ) file script that allows you to increase your windows pc's performance and tries to remove all existing bugs in your pc!

## How do I use Bug-Blitz?

**Using Bug-Blitz is quite easy!** 

1. After you install the Bug-Blitz.bat file make sure you right click it and run it as an administrator.
2. Before using any of the options in the script it is recommended to setup a restore point.
3. While downloading if you find any errors due to microsoft defender then you can disable microsoft defender until the process is complete and enable it again.
4. After launching Bug_Blitz you can choose which tweaks you want to apple.
5. After you have completed using Bug-Blitz, make sure to restart your pc to ensure that all tweaks are applied properly.

## Will it damage my computer?

No It won't! Bug-Blitz uses well known command prompt commands which are known to be safe and will not damage your computer, it's still recommended to setup a restore point as shown above.

## Is it a virus?

No Bug_Blitz is not a virus, It is a batch file so any 'virus' that you may think it has is false as you can download it and view to code for yourself. Still not relieved? Well here is a virus total scan if you're interested! https://www.virustotal.com/gui/file/bf148e783578376b6016d898064ec0361a47e1fbc531ba59eaed5b918213eaeb/detection

